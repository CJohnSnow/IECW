import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    { // 首页--张小敏
      path: '/',
      component: resolve => require(['@/views/Home/home'], resolve),
      name: 'HOME',
      meta: {
        title: 'Home'
      }
    },
    {
      path: '/ForeignNews',
      component: resolve => require(['@/views/Home/HomeDetails/ForeignNews/foreignnews'], resolve),
      name: 'Foreign Affairs News',
      meta: {
        title: 'Foreign News'
      }
    },
    {
      path: '/NoticeAnnouncement',
      component: resolve => require(['@/views/Home/HomeDetails/NoticeAnnouncement/noticeannouncement'], resolve),
      name: 'Notice and Announcement',
      meta: {
        title: 'Notice Announcement'
      }
    },
    { // 张小敏--关于我们（一级导航）
      path: '/ABOUT SAU',
      component: resolve => require(['@/components/common'], resolve),
      name: 'ABOUT SAU',
      redirect: '/College Overview',
      meta: {
        title: 'ABOUT SAU'
      },
      children: [{ // 张小敏--关于我们-学院概况(二级导航)
        path: '/College Overview',
        name: 'College Overview',
        component: resolve => require(['@/views/AboutSAU/CollegeOverview'], resolve),
        meta: {
          title: 'College Overview'
        }
      },
      { // 张小敏--关于我们-院长寄语（二级导航）
        path: '/President’s Welcome',
        name: 'President’s Welcome',
        component: resolve => require(['@/views/AboutSAU/PresidentWelcome'], resolve),
        meta: {
          title: 'President’s Welcome'
        }
      },
      { // 张小敏--关于我们-机构设置（二级导航）
        path: '/Administrative Offices',
        name: 'Administrative Offices',
        component: resolve => require(['@/views/AboutSAU/AdministrativeOffice'], resolve),
        meta: {
          title: 'Administrative Offices'
        }
      },
      { // 张小敏--关于我们-机构设置-现任领导（三级导航）
        path: '/CurrentLeader',
        name: 'CurrentLeader',
        component: resolve => require(['@/views/AboutSAU/AdminOfficeDetails/CurrentLeader'], resolve),
        meta: {
          title: 'CurrentLeader'
        }
      },
      { // 张小敏--关于我们-机构设置-行政办公室（三级导航）
        path: '/AdministrationOffice',
        name: 'AdministrationOffice',
        component: resolve => require(['@/views/AboutSAU/AdminOfficeDetails/AdministrationOffice'], resolve),
        meta: {
          title: 'AdministrationOffice'
        }
      },
      { // 张小敏--关于我们-科室设置（三级导航）
        path: '/DepartmentSetting',
        name: 'DepartmentSetting',
        component: resolve => require(['@/views/AboutSAU/AdminOfficeDetails/DepartmentSetting'], resolve),
        meta: {
          title: 'DepartmentSetting'
        }
      }]
    },
    { // 任丽蓉--留学川农（一级导航）
      path: '/STUDY AT SAU',
      name: 'STUDY AT SAU',
      component: resolve => require(['@/components/common'], resolve),
      redirect: '/Program Category',
      meta: {
        title: 'STUDY AT SAU'
      },
      children: [{ // 任丽蓉--留学川农-项目类型（二级导航）
        path: '/Program Category',
        name: 'Program Category',
        component: resolve => require(['@/views/StudyatSAU/ProgramCategray/UndergraduateProgram'], resolve),
        meta: {
          title: 'Program Category'
        }
      },
      { // 任丽蓉--留学川农-项目类型-本科项目（三级导航）
        path: '/Undergraduate Program',
        name: 'Undergraduate Program',
        component: resolve => require(['@/views/StudyatSAU/ProgramCategray/UndergraduateProgram'], resolve),
        meta: {
          title: 'Undergraduate Program'
        }
      },
      { // 任丽蓉--留学川农-项目类型-研究项目（三级导航）
        path: '/Graduate Program',
        name: 'Graduate Program',
        component: resolve => require(['@/views/StudyatSAU/ProgramCategray/GraduateProgram'], resolve),
        meta: {
          title: 'Graduate Program'
        }
      },
      { // 任丽蓉--留学川农-项目类型-汉语项目（三级导航）
        path: '/Chinese Language Program',
        name: 'Chinese Language Program',
        component: resolve => require(['@/views/StudyatSAU/ProgramCategray/CLProgram'], resolve),
        meta: {
          title: 'Chinese Language Program'
        }
      },
      { // 任丽蓉--留学川农-项目类型-预科项目（三级导航）
        path: '/Prior Program',
        name: 'Prior Program',
        component: resolve => require(['@/views/StudyatSAU/ProgramCategray/PriorProgram'], resolve),
        meta: {
          title: 'Prior Program'
        }
      },
      { // 任丽蓉--留学川农-项目类型-预科项目（三级导航）
        path: '/Short term Training Program',
        name: 'Short term Training Program',
        component: resolve => require(['@/views/StudyatSAU/ProgramCategray/TrainningProgram'], resolve),
        meta: {
          title: 'Short term Training Program'
        }
      },
      { // 任丽蓉--留学川农-在线申请（二级导航）
        path: '/Online Application',
        name: 'Online Application',
        redirect: '/Online Application System',
        component: resolve => require(['@/views/StudyatSAU/OnlineApplication/ApplicationSystem'], resolve),
        meta: {
          title: 'Online Application'
        }
      },
      { // 任丽蓉--留学川农-在线申请-在线系统（三级导航）
        path: '/Online Application System',
        name: 'Online Application System',
        component: resolve => require(['@/views/StudyatSAU/OnlineApplication/ApplicationSystem'], resolve),
        meta: {
          title: 'Online Application System'
        }
      },
      { // 任丽蓉--留学川农-在线申请-系统说明（三级导航）
        path: '/Introduction of System',
        name: 'Introduction of System',
        component: resolve => require(['@/views/StudyatSAU/OnlineApplication/IntroductionPage'], resolve),
        meta: {
          title: 'Introduction of System'
        }
      },
      { // 任丽蓉--留学川农-在线申请-奖学金（三级导航）
        path: '/Scholarship',
        name: 'Scholarship',
        component: resolve => require(['@/views/StudyatSAU/OnlineApplication/Scholarship'], resolve),
        meta: {
          title: 'Scholarship'
        }
      },
      { // 任丽蓉--留学川农-在线申请-规章制度（三级导航）
        path: '/Regulations',
        name: 'Regulations',
        component: resolve => require(['@/views/StudyatSAU/OnlineApplication/Regulations'], resolve),
        meta: {
          title: 'Regulations'
        }
      },
      { // 任丽蓉--留学川农-在线申请-报名专区（三级导航）
        path: '/Registration Area',
        name: 'Registration Area',
        component: resolve => require(['@/views/StudyatSAU/OnlineApplication/RegistrationArea'], resolve),
        meta: {
          title: 'Registration Area'
        }
      }]
    },
    { // 张小敏--研究中心（一级导航）
      path: '/RESEARCH CENTER',
      name: 'RESEARCH CENTER',
      component: resolve => require(['@/components/common'], resolve),
      redirect: '/Introduction of SAU',
      meta: {
        title: 'RESEARCH CENTER'
      },
      children: [{ // 张小敏--研究中心-川农简介（二级导航）
        path: '/Introduction of SAU',
        name: 'Introduction of SAU',
        component: resolve => require(['@/views/ResearchCenter/IntroductionOfSAU'], resolve),
        meta: {
          title: 'Introduction of SAU'
        }
      },
      { // 张小敏--研究中心-重点学科（二级导航）
        path: '/Key Disciplines',
        name: 'Key Disciplines',
        component: resolve => require(['@/views/ResearchCenter/KeyDicipline'], resolve),
        meta: {
          title: 'Key Disciplines'
        }
      },
      { // 张小敏--研究中心-科研平台（二级导航）
        path: '/Research Platform',
        name: 'Research Platform',
        component: resolve => require(['@/views/ResearchCenter/ReserchPlatform'], resolve),
        meta: {
          title: 'Research Platform'
        }
      },
      { // 张小敏--研究中心-科研成果（二级导航）
        path: '/Research Achievements',
        name: 'Research Achievements',
        component: resolve => require(['@/views/ResearchCenter/ReasearchAchievement'], resolve),
        meta: {
          title: 'Research Achievements'
        }
      }]
    },
    { // 刘颖--合作办学（一级导航）
      path: '/COOPERATIVE EDUCATION',
      name: 'COOPERATIVE EDUCATION',
      component: resolve => require(['@/components/common'], resolve),
      redirect: '/Project Column',
      meta: {
        title: 'COOPERATIVE EDUCATION'
      },
      children: [{ // 刘颖--合作办学--项目专栏（二级导航）
        path: '/Project Column',
        name: 'Project Column',
        component: resolve => require(['@/views/CooperativeEducation/ProjectColumn'], resolve),
        meta: {
          title: 'Project Column'
        }
      },
      { // 刘颖--合作办学--申请流程（二级导航）
        path: '/Application Procedure',
        name: 'Application Procedure',
        component: resolve => require(['@/views/CooperativeEducation/ApplicationProcedure'], resolve),
        meta: {
          title: 'Application Procedure'
        }
      },
      { // 刘颖--合作办学--政策法规（二级导航）
        path: '/Polices and Regulations',
        name: 'Polices and Regulations',
        component: resolve => require(['@/views/CooperativeEducation/PolicesandRegulations'], resolve),
        meta: {
          title: 'Polices and Regulations'
        }
      },
      { // 刘颖--合作办学--常见问题（二级导航）
        path: '/Frequently Asked Questions',
        name: 'Frequently Asked Questions',
        component: resolve => require(['@/views/CooperativeEducation/FrequentlyAskedQuestions'], resolve),
        meta: {
          title: 'Frequently Asked Questions'
        }
      }]
    },
    { // 刘颖--培训中心（一级导航）
      path: '/TRAINING CENTER',
      name: 'TRAINING CENTER',
      component: resolve => require(['@/components/common'], resolve),
      redirect: '/Overview Of The Center',
      meta: {
        title: 'TRAINING CENTER'
      },
      children: [{ // 刘颖--培训中心--中心概况（二级导航）
        path: '/Overview Of The Center',
        name: 'Overview Of The Center',
        component: resolve => require(['@/views/TrainingCenter/OverViewoftheCenter'], resolve),
        meta: {
          title: 'Overview Of The Center'
        }
      },
      { // 刘颖--培训中心--中外专家联合（二级导航）
        path: '/Joint of Chinese and Foreign Experts',
        name: 'Joint of Chinese and Foreign Experts',
        component: resolve => require(['@/views/TrainingCenter/JointofChineseandForeginExerts'], resolve),
        meta: {
          title: 'Joint of Chinese and Foreign Experts'
        }
      },
      { // 刘颖--培训中心--项目宣传（二级导航）
        path: '/Project propagandize',
        name: 'Project propagandize',
        component: resolve => require(['@/views/TrainingCenter/ProjectPropaganize'], resolve),
        meta: {
          title: 'Project propagandize'
        }
      },
      { // 刘颖--培训中心--项目新闻（二级导航）
        path: '/Project News',
        name: 'Project News',
        component: resolve => require(['@/views/TrainingCenter/ProjectNews'], resolve),
        meta: {
          title: 'Project News'
        }
      }]
    },
    { // 童鸣--校园生活（一级导航）
      path: '/CAMPUS LIFE',
      name: 'CAMPUS LIFE',
      component: resolve => require(['@/components/common'], resolve),
      redirect: '/Campus Activities',
      meta: {
        title: 'CAMPUS LIFE'
      },
      children: [{ // 童鸣--校园生活--校园活动（二级导航）
        path: '/Campus Activities',
        name: 'Campus Activities',
        component: resolve => require(['@/views/CampusLife/CampusActivities'], resolve),
        meta: {
          title: 'Campus Activities'
        }
      },
      { // 童鸣--校园生活--学院风采（二级导航）
        path: '/Campus Gallery',
        name: 'Campus Gallery',
        component: resolve => require(['@/views/CampusLife/CampusGallery'], resolve),
        meta: {
          title: 'Campus Gallery'
        }
      },
      { // 童鸣--校园生活--校历（二级导航）
        path: '/Calendar',
        name: 'Calendar',
        component: resolve => require(['@/views/CampusLife/Calendar'], resolve),
        meta: {
          title: 'Calendar'
        }
      }]
    },
    { // 童鸣--公共服务（一级导航）
      path: '/PUBLICK SERVICE',
      name: 'PUBLICK SERVICE',
      component: resolve => require(['@/components/common'], resolve),
      redirect: '/Contact Us',
      meta: {
        title: 'PUBLICK SERVICE'
      },
      children: [{ // 童鸣--公共服务--联系我们
        path: '/Contact Us',
        name: 'Contact Us',
        component: resolve => require(['@/views/PublicService/ContactUs'], resolve),
        meta: {
          title: 'Contact Us'
        }
      },
      { // 童鸣--公共服务--相关政策
        path: '/Relevant Policies',
        name: 'Relevant Policies',
        component: resolve => require(['@/views/PublicService/RelevantPolicies'], resolve),
        meta: {
          title: 'Relevant Policies'
        }
      },
      { // 童鸣--公共服务--相关政策
        path: '/Download Zone',
        name: 'Download Zone',
        component: resolve => require(['@/views/PublicService/DownloadZone'], resolve),
        meta: {
          title: 'Download Zone'
        }
      }]
    },
    {
      path: '/PictureContent',
      component: resolve => require(['@/components/DetailsContent/picturecontent'], resolve),
      name: 'PictureContent',
      meta: {
        title: 'PictureContent'
      }
    },
    {
      path: '/TextContent',
      component: resolve => require(['@/components/DetailsContent/textcontent'], resolve),
      name: 'TextContent',
      meta: {
        title: 'TextContent'
      }
    }
  ],
  scrollBehavior (to, from, savedPosition) { // 打开的每一页的位置都不会相互影响
    return { x: 0, y: 0 }
  }
})
