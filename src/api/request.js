
import axios from 'axios'
import { Message } from 'element-ui'

axios.defaults.withCredentials = true
const instance = axios.create({
  baseURL: process.env.NODE_ENV === 'development'
    ? 'http://47.98.244.28:8081'
    : 'http://47.98.244.28:8081',
  timeout: 5000
})

instance.interceptors.request.use(
  config => {
    return config
  },
  error => {
    console.log(error)
    return Promise.reject(error)
  }
)

instance.interceptors.response.use(
  res => {
    return res.data
  },
  error => {
    console.log(error) // for debug
    if (error && error.response) {
      switch (error.response.status) {
        case 400:
          Message.error('400')
          break
          // 401: 未登录,跳转至登录页
        case 401:
          Message.error('401')
          break
        case 403:
          Message.error('403')
          break
          // 404请求不存在
        case 404:
          break
        case 500:
          Message.error('服务器故障')
          break
        default:
          break
      }
      return Promise.reject(error.response)
    }
  }
)

export default instance
