import axios from 'axios'

export function getCardInfo () {
  return axios.get('mock/card.json', {
    params: null
  }).then((res) => {
    return Promise.resolve(res.data)
  })
}
