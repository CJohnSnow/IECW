import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    currentNav: 'HOME',
    currentSubNav: '',
    currentThirdNav: '',
    currentPage: '',
    navItems: [
      {
        index: '1',
        title: 'HOME',
        active: true,
        children: [
          {
            index: '1-1',
            title: 'Foreign Affairs News',
            active: false
          },
          {
            index: '1-2',
            title: 'Notice and Announcement',
            active: false
          }
        ]
      },
      {
        index: '2',
        title: 'ABOUT SAU',
        active: false,
        children: [
          {
            index: '2-1',
            title: 'College Overview',
            active: false
          },
          {
            index: '2-2',
            title: 'President’s Welcome',
            active: false
          },
          {
            index: '2-3',
            title: 'Administrative Offices',
            active: false
          }
        ]
      },
      {
        index: '3',
        title: 'STUDY AT SAU', // 留学川农导航有待完善
        active: false,
        children: [
          {
            index: '3-1',
            title: 'Program Category',
            active: false,
            children: [
              {
                index: '3-1-1',
                title: 'Undergraduate Program',
                active: false
              },
              {
                index: '3-1-2',
                title: 'Graduate Program',
                active: false
              },
              {
                index: '3-1-3',
                title: 'Chinese Language Program',
                active: false
              },
              {
                index: '3-1-4',
                title: 'Prior Program',
                active: false
              },
              {
                index: '3-1-5',
                title: 'Short term Training Program',
                active: false
              }
            ]
          },
          {
            index: '3-2',
            title: 'Online Application',
            active: false,
            children: [
              {
                index: '3-2-1',
                title: 'Online Application System',
                active: false
              },
              {
                index: '3-2-2',
                title: 'Introduction of System',
                active: false
              },
              {
                index: '3-2-3',
                title: 'Scholarship',
                active: false
              },
              {
                index: '3-2-4',
                title: 'Regulations',
                active: false
              },
              {
                index: '3-2-5',
                title: 'Registration Area',
                active: false
              }
            ]
          }
        ]
      },
      {
        index: '4',
        title: 'RESEARCH CENTER',
        active: false,
        children: [
          {
            index: '4-1',
            title: 'Introduction of SAU',
            active: false
          },
          {
            index: '4-2',
            title: 'Key Disciplines',
            active: false
          },
          {
            index: '4-3',
            title: 'Research Platform',
            active: false
          },
          {
            index: '4-4',
            title: 'Research Achievements',
            active: false
          }
        ]
      },
      {
        index: '5',
        title: 'COOPERATIVE EDUCATION',
        active: false,
        children: [
          {
            index: '5-1',
            title: 'Project Column',
            active: false
          },
          {
            index: '5-2',
            title: 'Application Procedure',
            active: false
          },
          {
            index: '5-3',
            title: 'Polices and Regulations',
            active: false
          },
          {
            index: '5-4',
            title: 'Frequently Asked Questions',
            active: false
          }
        ]
      },
      {
        index: '6',
        title: 'TRAINING CENTER',
        active: false,
        children: [
          {
            index: '6-1',
            title: 'Overview Of The Center',
            active: false
          },
          {
            index: '6-2',
            title: 'Joint of Chinese and Foreign Experts',
            active: false
          },
          {
            index: '6-3',
            title: 'Project propagandize',
            active: false
          },
          {
            index: '6-4',
            title: 'Project News',
            active: false
          }
        ]
      },
      {
        index: '7',
        title: 'CAMPUS LIFE',
        active: false,
        children: [
          {
            index: '7-1',
            title: 'Campus Activities',
            active: false
          },
          {
            index: '7-2',
            title: 'Campus Gallery',
            active: false
          },
          {
            index: '7-3',
            title: 'Calendar',
            active: false
          }
        ]
      },
      {
        index: '8',
        title: 'PUBLICK SERVICE',
        active: false,
        children: [
          {
            index: '8-1',
            title: 'Contact Us',
            active: false
          },
          {
            index: '8-2',
            title: 'Relevant Policies',
            active: false
          },
          {
            index: '8-3',
            title: 'Download Zone',
            active: false
          }
        ]
      }
    ]
  },
  actions: {},
  mutations: {
    changeNav (state, navTitle) { // 改变一级导航状态
      state.currentNav = navTitle
      state.currentThirdNav = ''
      console.log(state.navItems)
      state.navItems.forEach((item, i) => {
        if (item.title === navTitle) {
          item.active = true
          state.currentSubNav = item.children[0].title
          item.children[0].active = true
        } else {
          item.active = false
        }
      })
      console.log(state.navItems)
    },
    changeSubNav (state, titles) { // 改变二级导航状态
      console.log(titles.navTitle)
      state.currentNav = titles.navTitle
      state.currentSubNav = titles.subTitle
      state.currentThirdNav = titles.thirdTitle
      state.navItems.forEach((item) => {
        if (item.title === titles.navTitle) {
          item.children.forEach((subItem) => {
            if (subItem.title === titles.subTitle) {
              subItem.active = true
            } else {
              subItem.active = false
            }
          })
        }
      })
      console.log(state.navItems)
    },
    changeThirdNav (state, titles) { // 改变三级导航状态
      state.currentNav = titles.navTitle
      state.currentSubNav = titles.subTitle
      state.currentThirdNav = titles.thirdTitle
      state.navItems.forEach((item) => {
        if (item.title === titles.navTitle) {
          item.children.forEach((subItem) => {
            if (subItem.title === titles.subTitle) {
              subItem.children.forEach((thirdItem) => {
                if (thirdItem.title === titles.thirdTitle) {
                  thirdItem.active = true
                } else {
                  thirdItem.active = false
                }
              })
            }
          })
        }
      })
      console.log(state.navItems)
    }
  }
})
